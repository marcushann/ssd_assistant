﻿Imports System.Text
Imports System.IO

Public Class move_form

    Private Property origin As String
    Dim parentFolder As String
    Private Property destination As String

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If origin_browser.ShowDialog() = Windows.Forms.DialogResult.OK Then
            origin = origin_browser.SelectedPath
            parentFolder = IO.Path.GetFileName(origin)
            TextBox1.Text = origin
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If destination_browser.ShowDialog() = Windows.Forms.DialogResult.OK Then
            destination = destination_browser.SelectedPath
            TextBox2.Text = destination
        End If
    End Sub

    Private Sub TextBox2_TextChanged(sender As Object, e As EventArgs) Handles TextBox2.TextChanged
        destination = TextBox2.Text
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        origin = TextBox1.Text
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        My.Computer.FileSystem.CopyDirectory(origin, destination, True)
        System.IO.Directory.Delete(origin, True)
        Process.Start("cmd", "/k mklink /d """ + origin + """ """ + destination + parentFolder + """")
        My.Settings.origin_folders.Add(origin)
        My.Settings.destination_folders.Add(destination)
        Dim folders As New Dictionary(Of String, String)
        Try
            Dim root As XElement = XElement.Load(Application.StartupPath & "\files.xml")
            For Each el As XElement In root.Elements
                Try
                    Dim folders_tmp_array() As String
                    folders_tmp_array = el.Value.Split(New Char() {";"c})
                    folders.Add(folders_tmp_array(0), folders_tmp_array(1))
                Catch ex As Exception

                End Try
            Next
            For Each str As String In folders.Keys
                Console.WriteLine("{0}:{1}", str, folders(str))
            Next
            folders.Add(origin, destination)
        Catch ex As Exception

        End Try
        
        Dim sbService As New StringBuilder

        sbService.AppendLine("<?xml version=""1.0""?>")
        sbService.AppendLine("<folders>")

        For Each item As KeyValuePair(Of String, String) In folders
            Dim key = item.Key
            Dim value = item.Value
            sbService.AppendFormat("   <folder>{0};{1}</folder>", key, value.ToString()).AppendLine()
        Next
        sbService.AppendLine("</folders>")
        Using outfile As New StreamWriter(Application.StartupPath & "\files.xml")
            outfile.Write(sbService.ToString())
        End Using

    End Sub


    Private Sub move_form_Load(sender As Object, e As EventArgs)

    End Sub
End Class