﻿Imports System.Collections.Specialized


Public Class main

    Dim folders As New Dictionary(Of String, String)

    Private Sub Move_Click(sender As Object, e As EventArgs) Handles Move.Click
        move_form.ShowDialog()
        updateList()
    End Sub

    Private Sub main_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        updateList()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)
        My.Settings.origin_folders.Clear()
        My.Settings.destination_folders.Clear()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        move_back.ShowDialog()
        updateList()
    End Sub

    Private Sub updateList()
        ListBox1.Items.Clear()
        ListBox2.Items.Clear()
        folders.Clear()
        Try
            Dim root As XElement = XElement.Load(Application.StartupPath & "\files.xml")
            For Each el As XElement In root.Elements
                Try
                    Dim folders_tmp_array() As String
                    folders_tmp_array = el.Value.Split(New Char() {";"c})
                    folders.Add(folders_tmp_array(0), folders_tmp_array(1))
                Catch ex As Exception

                End Try
            Next
            For Each str As String In folders.Keys
                ListBox1.Items.Add(str)
                ListBox2.Items.Add(folders(str))
                Console.WriteLine("{0}:{1}", str, folders(str))
            Next
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        updateList()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Settings.Show()
    End Sub
End Class