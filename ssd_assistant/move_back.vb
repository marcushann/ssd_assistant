﻿Imports System.Text
Imports System.IO

Public Class move_back

    Dim folders As New Dictionary(Of String, String)
    Dim currentitem As String
    Dim selected As String()

    Private Property parentFolder As String

    Private Sub move_back_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        updateList()
    End Sub

    Private Sub ListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBox1.SelectedIndexChanged
        currentitem = ListBox1.SelectedItem.ToString
        selected = currentitem.Split(New Char() {">"c})
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim origin = selected(1)
        Dim destination = selected(0)
        parentFolder = IO.Path.GetFileName(origin)
        System.IO.Directory.Delete(destination, True)
        My.Computer.FileSystem.CopyDirectory(origin, destination, True)
        System.IO.Directory.Delete(origin, True)
        Dim folders As New Dictionary(Of String, String)
        Try
            Dim root As XElement = XElement.Load(Application.StartupPath & "\files.xml")
            For Each el As XElement In root.Elements
                Try
                    Dim folders_tmp_array() As String
                    folders_tmp_array = el.Value.Split(New Char() {";"c})
                    folders.Add(folders_tmp_array(0), folders_tmp_array(1))
                Catch ex As Exception

                End Try
            Next
            For Each str As String In folders.Keys
                Console.WriteLine("{0}:{1}", str, folders(str))
            Next
            folders.Remove(destination)

        Catch ex As Exception

        End Try

        Dim sbService As New StringBuilder

        sbService.AppendLine("<?xml version=""1.0""?>")
        sbService.AppendLine("<folders>")

        For Each item As KeyValuePair(Of String, String) In folders
            Dim key = item.Key
            Dim value = item.Value
            sbService.AppendFormat("   <folder>{0};{1}</folder>", key, value.ToString()).AppendLine()
        Next
        sbService.AppendLine("</folders>")
        Using outfile As New StreamWriter(Application.StartupPath & "\files.xml")
            outfile.Write(sbService.ToString())
        End Using
        updateList()
    End Sub

    Private Sub updateList()
        ListBox1.Items.Clear()
        folders.Clear()
        Try
            Dim root As XElement = XElement.Load(Application.StartupPath & "\files.xml")
            For Each el As XElement In root.Elements
                Try
                    Dim folders_tmp_array() As String
                    folders_tmp_array = el.Value.Split(New Char() {";"c})
                    folders.Add(folders_tmp_array(0), folders_tmp_array(1))
                Catch ex As Exception

                End Try
            Next
            For Each str As String In folders.Keys
                ListBox1.Items.Add(str + ">" + folders(str))
                Console.WriteLine("{0}:{1}", str, folders(str))
            Next
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
    End Sub
End Class